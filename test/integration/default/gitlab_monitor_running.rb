require 'inspec'

control 'gitlab-monitor installation' do
  # These runit services aren't systemd managed :(
  # describe service('gitlab-monitor') do
  #   it { should be_installed }
  #   it { should be_enabled }
  #   it { should be_running }
  # end
  
  describe processes(Regexp.new('gitlab-monitor')) do
    it { should exist }
    its('entries.length') { should eq 3 } # runsv gitlab-monitor / svlogd .. / ruby gem execution 
  end
end

