require 'inspec'

control 'redis monitor configuration file' do
  describe file('/opt/gitlab-monitor/config/redis-config.yml') do
    its('content') { should match(/testingpassword/) }
  end
end

