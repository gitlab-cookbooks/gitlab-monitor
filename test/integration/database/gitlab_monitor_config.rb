require 'inspec'

control 'database monitor configuration file' do
  describe file('/opt/gitlab-monitor/config/database-config.yml') do
    its('content') { should match(/hunter3/) }
  end
end

