require 'spec_helper'
require 'chef-vault'

describe 'gitlab-monitor::database' do
  let(:data_bag) { 'gitlab-cluster-base' }
  let(:data_bag_item) { 'prd' }
  let(:secrets) { { 'db_role_password' => 'hunter2' } }
  let(:psql_executable) { '/bin/psql' }
  let(:pg_host) { '/var/psql' }
  let(:psql_commnad) { "#{psql_executable} -h #{pg_host} postgres" }
  let(:chef_run) do
    ChefSpec::SoloRunner.new do |node|
      node.normal['gitlab_monitor']['chef_vault'] = data_bag
      node.normal['gitlab_monitor']['chef_vault_item'] = data_bag_item
      node.normal['gitlab_monitor']['database']['psql_executable'] = psql_executable
      node.normal['gitlab_monitor']['database']['pg_host'] = pg_host
    end.converge(described_recipe)
  end

  before do
    expect_any_instance_of(Chef::Recipe).to receive(:chef_vault_item)
      .with(data_bag, data_bag_item).and_return(secrets)

    stub_command("#{psql_commnad} -c \"SELECT 1\"").and_return(true)
    stub_command("#{psql_commnad} -c \"SELECT 'found' FROM pg_roles WHERE rolname='gitlab-monitor'\" | grep found").and_return(false)
  end

  it 'converges successfully' do
    expect { chef_run }.to_not raise_error
  end
end
