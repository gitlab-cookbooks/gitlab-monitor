require 'spec_helper'
require 'chef-vault'

describe 'gitlab-monitor::redis' do
  let(:redis_config_path) { '/opt/gitlab-monitor/config/redis-config.yml' }
  let(:redis_config) do
    { 'probes': { 'opts': { 'redis_url': 'redis://:<password>@localhost/' } } }
  end
  let(:redis_password) { 'hunter2' }
  let(:alt_redis_password) { 'hunter3' }
  let(:alt_redis_username) { 'redis-user' }
  let(:alt_path) { 'alt_redis' }
  let(:username) { '' }
  let(:password) { redis_password }
  let(:secrets) do
    {
      'omnibus-gitlab' => {
        'gitlab_rb' => {
          'redis' => { 'password' => redis_password },
          'alt_redis' => { 'password' => alt_redis_password },
        },
      },
    }
  end

  shared_examples 'configuring redis' do
    it 'creates the redis configuration with the correct password' do
      expect(chef_run).to create_file(redis_config_path).with(
        content: <<YAML
---
probes:
  opts:
    redis_url: redis://#{username}:#{password}@localhost/
YAML
      )
    end
  end

  shared_context 'ACL details' do
    let(:username) { alt_redis_username }
    let(:redis_config) do
      { 'probes': { 'opts': { 'redis_url': 'redis://<username>:<password>@localhost/' } } }
    end
  end

  context 'credentials in Chef vault' do
    let(:data_bag) { 'gitlab-cluster-base' }
    let(:data_bag_item) { 'prd' }
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab_monitor']['redis']['config_path'] = redis_config_path
        node.normal['gitlab_monitor']['redis']['config'] = redis_config
        node.normal['gitlab_monitor']['chef_vault'] = data_bag
        node.normal['gitlab_monitor']['chef_vault_item'] = data_bag_item
      end.converge(described_recipe)
    end

    before do
      expect_any_instance_of(Chef::Recipe).to receive(:chef_vault_item)
        .with(data_bag, data_bag_item).and_return(secrets)
    end

    it_behaves_like 'configuring redis'

    context 'with alternate path' do
      let(:password) { alt_redis_password }
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['gitlab_monitor']['redis']['config_path'] = redis_config_path
          node.normal['gitlab_monitor']['redis']['config'] = redis_config
          node.normal['gitlab_monitor']['redis']['credential_path'] = alt_path
          node.normal['gitlab_monitor']['chef_vault'] = data_bag
          node.normal['gitlab_monitor']['chef_vault_item'] = data_bag_item
        end.converge(described_recipe)
      end

      it_behaves_like 'configuring redis'
    end

    context 'with redis ACL' do
      include_context 'ACL details'

      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['gitlab_monitor']['redis']['config_path'] = redis_config_path
          node.normal['gitlab_monitor']['redis']['config'] = redis_config
          node.normal['gitlab_monitor']['redis']['username'] = username
          node.normal['gitlab_monitor']['chef_vault'] = data_bag
          node.normal['gitlab_monitor']['chef_vault_item'] = data_bag_item
        end.converge(described_recipe)
      end

      it_behaves_like 'configuring redis'
    end
  end

  context 'credentials in a backend specified by attributes' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab_monitor']['redis']['config_path'] = redis_config_path
        node.normal['gitlab_monitor']['redis']['config'] = redis_config
        node.normal['gitlab_monitor']['secrets'] = {
          'backend' => 'gkms',
          'path' => 'some-path',
          'key' => 'some-key',
        }
      end.converge(described_recipe)
    end

    before do
      expect_any_instance_of(Chef::Recipe).to receive(:get_secrets)
        .with('gkms', 'some-path', 'some-key').and_return(secrets)
    end

    it_behaves_like 'configuring redis'

    context 'with alternate path' do
      let(:password) { alt_redis_password }
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['gitlab_monitor']['redis']['config_path'] = redis_config_path
          node.normal['gitlab_monitor']['redis']['credential_path'] = alt_path
          node.normal['gitlab_monitor']['redis']['config'] = redis_config
          node.normal['gitlab_monitor']['secrets'] = {
            'backend' => 'gkms',
            'path' => 'some-path',
            'key' => 'some-key',
          }
        end.converge(described_recipe)
      end

      it_behaves_like 'configuring redis'
    end

    context 'with redis ACL' do
      include_context 'ACL details'

      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['gitlab_monitor']['redis']['config_path'] = redis_config_path
          node.normal['gitlab_monitor']['redis']['username'] = username
          node.normal['gitlab_monitor']['redis']['config'] = redis_config
          node.normal['gitlab_monitor']['secrets'] = {
            'backend' => 'gkms',
            'path' => 'some-path',
            'key' => 'some-key',
          }
        end.converge(described_recipe)
      end

      it_behaves_like 'configuring redis'
    end
  end
end
