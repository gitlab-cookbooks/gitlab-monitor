require 'spec_helper'
require 'chef-vault'

describe 'gitlab-monitor::worker' do
  let(:chef_run) do
    ChefSpec::SoloRunner.new.converge(described_recipe)
  end

  it 'converges successfully' do
    expect { chef_run }.to_not raise_error
  end
end
