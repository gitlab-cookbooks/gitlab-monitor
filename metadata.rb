name             'gitlab-monitor'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license          'All rights reserved'
description      'Installs/Configures gitlab-monitor'
version          '1.3.0'

depends 'ruby_build', '~> 1.3.0'
depends 'runit'
depends 'chef-vault' # rubocop:disable Chef/Modernize/DependsOnChefVaultCookbook
depends 'gitlab_secrets'
depends 'postgresql', '~> 6.1.4'
# For Chef 14 compatiblitiy
depends 'git', '~> 10.1'
depends 'seven_zip', '~> 2.0.2'
