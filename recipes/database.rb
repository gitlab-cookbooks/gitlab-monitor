require 'yaml'

include_recipe 'gitlab-monitor::default'

secrets =
  if node['gitlab_monitor']['chef_vault']
    include_recipe 'chef-vault'
    chef_vault_item = node['gitlab_monitor']['chef_vault_item'] || node.chef_environment

    chef_vault_item(node['gitlab_monitor']['chef_vault'], chef_vault_item)
  else
    secrets_hash = node['gitlab_monitor']['secrets']

    get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
  end

role_password   = secrets['db_role_password']
create_role_sql = %(CREATE ROLE \\"gitlab-monitor\\" INHERIT SUPERUSER LOGIN PASSWORD '#{role_password}')
psql_command    = "#{node['gitlab_monitor']['database']['psql_executable']} -h #{node['gitlab_monitor']['database']['pg_host']} postgres"

execute 'create role' do
  command "#{psql_command} -c \"#{create_role_sql}\""
  user node['gitlab_monitor']['database']['pg_superuser']
  only_if "#{psql_command} -c \"SELECT 1\"" # Only if we can execute queries on this machine
  not_if "#{psql_command} -c \"SELECT 'found' FROM pg_roles WHERE rolname='gitlab-monitor'\" | grep found"
end

config = node['gitlab_monitor']['database'].fetch('config') { node['gitlab_monitor']['database']['probes_config'] }
file node['gitlab_monitor']['database']['config_path'] do
  content YAML.dump(config.to_hash).gsub('<password>', role_password)
  mode '0755'
  owner node['gitlab_monitor']['user']
  group node['gitlab_monitor']['group']
  notifies :restart, 'runit_service[gitlab-monitor]', :delayed
end

include_recipe 'runit::default'
runit_service 'gitlab-monitor' do
  options({
    service: 'database',
  })
  default_logger true
  log_dir node['gitlab_monitor']['log_dir']
  subscribes :restart, 'gem_package[gitlab-exporter]', :delayed
  sv_timeout 180
end
