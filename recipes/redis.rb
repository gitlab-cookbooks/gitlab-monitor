require 'yaml'

include_recipe 'gitlab-monitor::default'

secrets =
  if node['gitlab_monitor']['chef_vault']
    include_recipe 'chef-vault'
    chef_vault_item = node['gitlab_monitor']['chef_vault_item'] || node.chef_environment

    chef_vault_item(node['gitlab_monitor']['chef_vault'], chef_vault_item)
  else
    secrets_hash = node['gitlab_monitor']['secrets']

    get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
  end

cred_path = node['gitlab_monitor']['redis']['credential_path']
redis_pw = secrets['omnibus-gitlab']['gitlab_rb'][cred_path]['password']
redis_user = node['gitlab_monitor']['redis']['username'].to_s

config = node['gitlab_monitor']['redis'].fetch('config') { node['gitlab_monitor']['redis']['probes_config'] }
file node['gitlab_monitor']['redis']['config_path'] do
  content YAML.dump(config.to_hash).gsub('<password>', redis_pw).gsub('<username>', redis_user)
  mode '0600'
  owner node['gitlab_monitor']['user']
  group node['gitlab_monitor']['group']
  notifies :restart, 'runit_service[gitlab-monitor]', :delayed
end

include_recipe 'runit::default'
runit_service 'gitlab-monitor' do
  options({
    service: 'redis',
  })
  default_logger true
  log_dir node['gitlab_monitor']['log_dir']
  subscribes :restart, 'gem_package[gitlab-exporter]', :delayed
end
