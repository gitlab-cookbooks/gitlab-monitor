require 'yaml'

include_recipe 'gitlab-monitor::default'

config = node['gitlab_monitor']['worker'].fetch('config') { node['gitlab_monitor']['worker']['probes_config'] }
file node['gitlab_monitor']['worker']['config_path'] do
  content YAML.dump(config.to_hash)
  mode '0755'
  owner node['gitlab_monitor']['user']
  group node['gitlab_monitor']['group']
  notifies :restart, 'runit_service[gitlab-monitor]', :delayed
end

include_recipe 'runit::default'
runit_service 'gitlab-monitor' do
  options({
    service: 'worker',
  })
  default_logger true
  log_dir node['gitlab_monitor']['log_dir']
  subscribes :restart, 'gem[gitlab-exporter]', :delayed
end
