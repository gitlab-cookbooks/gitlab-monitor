#
# Cookbook:: gitlab-monitor
# Recipe:: default
#
# Copyright:: (C) 2016 GitLab B.V.
#
# All rights reserved - Do Not Redistribute
#

apt_update 'apt-get update' do
  action :update
end if platform_family?('debian')

package 'runit-run' if node['platform_version'].to_i > 20

unless node['gitlab_monitor']['use_embedded_ruby']
  include_recipe 'ruby_build::default'

  ruby_build_ruby node['gitlab_monitor']['ruby_version'] do
    prefix_path node['gitlab_monitor']['ruby_prefix_path']
  end
end

user node['gitlab_monitor']['user'] do
  system true
  shell '/bin/false'
  home node['gitlab_monitor']['install_dir']
  not_if node['gitlab_monitor']['user'] == 'root'
end

directory node['gitlab_monitor']['install_dir'] do
  owner node['gitlab_monitor']['user']
  group node['gitlab_monitor']['group']
  mode '0755'
  recursive true
end

node['gitlab_monitor']['bundler_versions'].each do |bundler_version|
  gem_package 'bundler' do
    gem_binary "#{node['gitlab_monitor']['ruby_prefix_path']}/bin/gem"
    version bundler_version
  end
end

package 'postgresql-server-dev-all'

gem_package 'gitlab-exporter' do
  gem_binary "#{node['gitlab_monitor']['ruby_prefix_path']}/bin/gem"
  version node['gitlab_monitor']['gitlab_exporter_version']
end

directory node['gitlab_monitor']['config_dir'] do
  owner node['gitlab_monitor']['user']
  group node['gitlab_monitor']['group']
  mode '0755'
  action :create
end
