# common_attrs = {
#   methods: ['probe_db'],
#   opts: {
#     connection_string: "dbname=gitlabhq_production user=gitlab-monitor password=<password> host=/var/opt/gitlab/postgresql"
#   }
# }

default['gitlab_monitor']['database']['config_path']   = "#{default['gitlab_monitor']['install_dir']}/config/database-config.yml"
default['gitlab_monitor']['database']['probes_config'] = {
  probes: {
    # database: {
    #   multiple: true,
    #   dead_tuples_count: {
    #     class_name: 'Database::DeadTuplesProber',
    #   }.merge(common_attrs),
    #   slow_queries: {
    #     class_name: 'Database::SlowQueriesProber',
    #   }.merge(common_attrs),
    #   blocked_queries: {
    #     class_name: 'Database::BlockedQueriesProber',
    #   }.merge(common_attrs),
    #   vacuum_queries: {
    #     class_name: 'Database::VacuumQueriesProber',
    #   }.merge(common_attrs),
    # },
    # db_rows_count: {
    #   class_name: 'Database::RowCountProber',
    # }.merge(common_attrs),
    # ci_builds: {
    #   class_name: 'Database::CiBuildsProber',
    # }.merge(common_attrs),
  },
}

default['gitlab_monitor']['database']['psql_executable'] = '/opt/gitlab/embedded/bin/psql'
default['gitlab_monitor']['database']['pg_host']         = '/var/opt/gitlab/postgresql'
default['gitlab_monitor']['database']['pg_superuser']    = 'gitlab-psql'
