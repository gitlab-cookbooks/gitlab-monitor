default['gitlab_monitor']['redis']['config_path'] = "#{default['gitlab_monitor']['config_dir']}/redis-config.yml"
default['gitlab_monitor']['redis']['credential_path'] = 'redis'
default['gitlab_monitor']['redis']['username'] = nil
default['gitlab_monitor']['redis']['probes_config'] = {
  probes: {
    sidekiq: {
      methods: %w(
        probe_queues
        probe_jobs
        probe_workers
        probe_retries
        probe_dead
      ),
      opts: {
        redis_url: 'redis://:<password>@localhost:6379/',
      },
    },
  },
}
