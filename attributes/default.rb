default['gitlab_monitor']['bundler_versions'] = ['2.2.22']
default['gitlab_monitor']['ruby_version'] = '2.7.0'
default['gitlab_monitor']['use_embedded_ruby'] = Chef::VERSION.to_i > 15
default['gitlab_monitor']['ruby_prefix_path'] =
  if node['gitlab_monitor']['use_embedded_ruby']
    '/opt/cinc/embedded'
  else
    "/opt/ruby-#{default['gitlab_monitor']['ruby_version']}"
  end
default['gitlab_monitor']['gitlab_exporter_version'] = '11.11.0'
default['gitlab_monitor']['install_dir']             = '/opt/gitlab-monitor'
default['gitlab_monitor']['config_dir']              = '/opt/gitlab-monitor/config'
default['gitlab_monitor']['log_dir']                 = '/var/log/gitlab-monitor'
default['gitlab_monitor']['executable']              = "#{default['gitlab_monitor']['ruby_prefix_path']}/bin/gitlab-exporter"
default['gitlab_monitor']['user']                    = 'gitlab-monitor'
default['gitlab_monitor']['group']                   = 'gitlab-monitor'
default['gitlab_monitor']['secrets']['backend']      = 'chef_vault'
default['gitlab_monitor']['secrets']['path']         = 'gitlab-cluster-base'
default['gitlab_monitor']['secrets']['key']          = '_default'
default['ruby_build']['git_ref']                     = 'v20220125'
default['ruby_build']['upgrade']                     = true
