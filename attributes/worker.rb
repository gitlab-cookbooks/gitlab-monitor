default['gitlab_monitor']['worker']['config_path']   = "#{default['gitlab_monitor']['config_dir']}/worker-config.yml"
default['gitlab_monitor']['worker']['probes_config'] = {
  probes: {
    process: {
      multiple: true,
      process: {
        methods: %w(probe_memory probe_age probe_count),
        opts: [
          {
            pid_or_pattern: 'sidekiq .* \\\\[.*?\\\\]',
            name: 'sidekiq',
            quantiles: true,
          },
          {
            pid_or_pattern: 'unicorn worker\\\\[.*?\\\\]',
            name: 'unicorn',
            quantiles: true,
          },
          {
            pid_or_pattern: 'git-upload-pack --stateless-rpc',
            name: 'git_upload_pack_http',
            quantiles: true,
          },
          {
            pid_or_pattern: 'git-upload-pack /',
            name: 'git_upload_pack_ssh',
            quantiles: true,
          },
        ],
      },
      git_process: {
        methods: ['probe_git'],
        opts: {
          quantiles: true,
        },
      },
    },
  },
}
